FROM tensorflow/tensorflow

RUN apt-get update

RUN pip install keras
RUN pip install requests
RUN pip install statsmodels
RUN pip install diskcache
RUN pip install sqlalchemy
RUN pip install psycopg2

ENV TZ America/Toronto
RUN echo $TZ >  /etc/timezone && \
    apt-get update && apt-get install -y  tzdata && \
    dpkg-reconfigure -f noninteractive tzdata && \
    apt-get clean
